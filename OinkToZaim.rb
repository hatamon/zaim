require 'json'
require 'CSV'
require './Zaim'
require './DatabaseForConvert'
require './zaiminfo'

class OinkToZaim
private
	def initialize
		@titleToColumn = {}
		@zaim = Zaim.new(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
		zaimAccountListJson = @zaim.getAccountList
		zaimCategoryListJson = @zaim.getCategoryList
		zaimGenreListJson = @zaim.getGenreList
		oinkAccountToZaimTableJson = JSON.load(open('accountChange.json'))
		oinkCategoryToZaimTableJson = JSON.load(open('categoryChange.json'))
		@db = DatabaseForConvert.new(zaimAccountListJson, zaimCategoryListJson, zaimGenreListJson, oinkAccountToZaimTableJson, oinkCategoryToZaimTableJson)
	end

	def fieldValue(fields, title)
		return fields[@titleToColumn[title]]
	end

	def isTitleLine(fields)
		return fields[0] == '日付'
	end

	def isShishutsuLine(fields)
		return fieldValue(fields, '明細区分') == '支出'
	end

	def isShunyuLine(fields)
		return fieldValue(fields, '明細区分') == '収入'
	end

	def paramsForPayment(fields)
		oinkHimoku = fieldValue(fields, '費目')
		oinkAmount = fieldValue(fields, '出金')
		oinkDate = fieldValue(fields, '日付')
		oinkAccount = fieldValue(fields, '口座')
		oinkMemo = fieldValue(fields, 'メモ')
		oinkDetail = fieldValue(fields, '内容')
		oinkName = ''
		oinkPlace = ''

		zaimCategory = @db.oinkCategoryToZaim('payment', oinkHimoku)
		zaimCategoryIds = @db.getCategoryId('payment', zaimCategory['zaimCategory'])
		zaimCategoryId = zaimCategoryIds['local_id']
		zaimGenreId = @db.getGenreId(zaimCategoryIds['id'], zaimCategory['zaimGenre'])['local_id']
		zaimAccount = @db.oinkAccountToZaim(oinkAccount)['zaim']
		zaimAccountId = @db.getAccountId(zaimAccount)['local_id']

		return {
			'categoryId'=>zaimCategoryId,
			'genreId'=>zaimGenreId,
			'amount'=>oinkAmount,
			'date'=>oinkDate.gsub(/\//,"-"),
			'fromAccountId'=>zaimAccountId,
			'comment'=>oinkMemo + ' ' + oinkDetail,
			'name'=>oinkName,
			'place'=>oinkPlace
		}
	end

	def paramsForIncome(fields)
		oinkHimoku = fieldValue(fields, '費目')
		oinkAmount = fieldValue(fields, '入金')
		oinkDate = fieldValue(fields, '日付')
		oinkAccount = fieldValue(fields, '口座')
		oinkMemo = fieldValue(fields, 'メモ')
		oinkDetail = fieldValue(fields, '内容')

		zaimCategory = @db.oinkCategoryToZaim('income', oinkHimoku)
		zaimCategoryId = @db.getCategoryId('income', zaimCategory['zaimCategory'])['local_id']
		zaimAccount = @db.oinkAccountToZaim(oinkAccount)['zaim']
		zaimAccountId = @db.getAccountId(zaimAccount)['local_id']

		return {
			"categoryId"=>zaimCategoryId,
			"amount"=>oinkAmount,
			"date"=>oinkDate.gsub(/\//,"-"),
			"toAccountId"=>zaimAccountId,
			"comment"=>oinkMemo + ' ' + oinkDetail
		}
	end

	def paramsForTransfer(fromFields, toFields)
		oinkAmount = fieldValue(fromFields, '出金')
		oinkDate = fieldValue(fromFields, '日付')
		oinkFromAccount = fieldValue(fromFields, '口座')
		oinkToAccount = fieldValue(toFields, '口座')
		oinkMemo = fieldValue(fromFields, 'メモ')
		oinkDetail = fieldValue(fromFields, '内容')

		zaimFromAccount = @db.oinkAccountToZaim(oinkFromAccount)['zaim']
		zaimFromAccountId = @db.getAccountId(zaimFromAccount)['local_id']
		zaimToAccount = @db.oinkAccountToZaim(oinkToAccount)['zaim']
		zaimToAccountId = @db.getAccountId(zaimToAccount)['local_id']
		return {
			"amount"=>oinkAmount,
			"date"=>oinkDate.gsub(/\//,"-"),
			"fromAccountId"=>zaimFromAccountId,
			"toAccountId"=>zaimToAccountId,
			"comment"=>oinkMemo + ' ' + oinkDetail
		}
	end

	def init(oinkCsvPath)
		CSV.foreach(oinkCsvPath) do |fields|
			if isTitleLine(fields) then
				fields.each_with_index do |title, index|
					@titleToColumn[title] = index
				end
				break
			end
		end
	end

public
	def convert(oinkCsvPath)
		init(oinkCsvPath)
		needNextLine = false
		prevFields = nil

		CSV.foreach(oinkCsvPath) do |fields|
			if needNextLine then
				if prevFields == nil then
					raise 'Error : needNextLine なのに prevFields == nil : ' + fields.to_s
				end
				if isTitleLine(fields) then
					raise 'Error : needNextLine なのにタイトル行 : ' + fields.to_s
				end
				if !isShunyuLine(fields) then
					raise 'Error : needNextLine なのに収入じゃない : ' + fields.to_s
				end
				if fields[@titleToColumn["費目"]] != prevFields[@titleToColumn["費目"]] then
					raise "Error : needNextLine なのに費目が不一致 : #{prevFields} -> #{fields.to_s}"
				end
			end

			next if isTitleLine(fields)

			if isShishutsuLine(fields) then
				if fieldValue(fields, '費目') == '[チャージ]' || fieldValue(fields, '費目') == '[口座移動]' || fieldValue(fields, '費目') == '[引出]' || fieldValue(fields, '費目') == '[預入]' then
					needNextLine = true
					prevFields = fields
				elsif /^\[/.match(fieldValue(fields, '費目')) then
					raise '不明な費目 ' + fieldValue(fields, '費目')
				else
					print 'payment ...'
					params = paramsForPayment(fields)
					p params
					@zaim.payment(params)
					puts 'ok'

					needNextLine = false
					prevFields = nil
				end
			elsif isShunyuLine(fields) then
				if fieldValue(fields, '費目') == '[チャージ]' || fieldValue(fields, '費目') == '[口座移動]' || fieldValue(fields, '費目') == '[引出]' || fieldValue(fields, '費目') == '[預入]' then
					print 'transfer... '
					params = paramsForTransfer(prevFields, fields)
					p params
					@zaim.transfer(params)
					puts 'ok'

					needNextLine = false
					prevFields = nil
				elsif fieldValue(fields, '費目') == '[新規口座]' then
					print 'income '
					params = paramsForIncome(fields)
					p params
					@zaim.income(params)
					puts 'ok'

					needNextLine = false
					prevFields = nil
				elsif /^\[/.match(fieldValue(fields, '費目')) then
					raise '不明な費目 ' + fieldValue(fields, '費目')
				else
					puts 'income '
					params = paramsForIncome(fields)
					p params
					@zaim.income(params)
					puts 'ok'

					needNextLine = false
					prevFields = nil
				end
			else
				raise '不明な明細区分 ' + fields
			end
		end
	end

	def deleteAll()
		@zaim.getMoneyList()['money'].each {|record|
			p @zaim.delete(record["mode"], record["id"])
		}
	end
end
