require 'json'
require './Zaim'
require './DatabaseForRestore'
require './zaiminfo'

class BackupRestore
private
	def initialize
		@titleToColumn = {}
		@zaim = Zaim.new(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
		zaimAccountListJson = @zaim.getAccountList
		zaimCategoryListJson = @zaim.getCategoryList
		zaimGenreListJson = @zaim.getGenreList
		@db = DatabaseForRestore.new(zaimAccountListJson, zaimCategoryListJson, zaimGenreListJson)
	end

	def paramsForPayment(record)
		zaimCategoryId = @db.getCategoryLocalId(record['category_id'])['local_id']
		zaimGenreId = @db.getGenreLocalId(record['genre_id'])['local_id']
		zaimAccountId = @db.getAccountLocalId(record['from_account_id'])['local_id']

		return {
			'categoryId'=>zaimCategoryId,
			'genreId'=>zaimGenreId,
			'amount'=>record['amount'],
			'date'=>record['date'],
			'fromAccountId'=>zaimAccountId,
			'comment'=>record['comment'],
			'name'=>record['name'],
			'place'=>record['place']
		}
	end

	def paramsForIncome(record)
		zaimCategoryId = @db.getCategoryLocalId(record['category_id'])['local_id']
		zaimAccountId = @db.getAccountLocalId(record['to_account_id'])['local_id']

		return {
			'categoryId'=>zaimCategoryId,
			'amount'=>record['amount'],
			'date'=>record['date'],
			'toAccountId'=>zaimAccountId,
			'comment'=>record['comment']
		}
	end

	def paramsForTransfer(record)
		zaimFromAccountId = @db.getAccountLocalId(record['from_account_id'])['local_id']
		zaimToAccountId = @db.getAccountLocalId(record['to_account_id'])['local_id']
		return {
			'amount'=>record['amount'],
			'date'=>record['date'],
			'fromAccountId'=>zaimFromAccountId,
			'toAccountId'=>zaimToAccountId,
			'comment'=>record['comment']
		}
	end

public
	def backup(outputFolder)
		Dir::mkdir(outputFolder)
		open(outputFolder + '/money.json', 'w') {|file|
			file.write(JSON.pretty_generate(@zaim.getMoneyList()))
		}
		open(outputFolder + '/accounts.json', 'w') {|file|
			file.write(JSON.pretty_generate(@zaim.getAccountList()))
		}
		open(outputFolder + '/categories.json', 'w') {|file|
			file.write(JSON.pretty_generate(@zaim.getCategoryList()))
		}
		open(outputFolder + '/genres.json', 'w') {|file|
			file.write(JSON.pretty_generate(@zaim.getGenreList()))
		}
	end

	def restore(inputFolder)
		history = JSON.load(open(inputFolder + '/money.json'))['money']

		history.each {|record|
			if record['mode'] == 'payment' then
				print 'payment ...'
				params = paramsForPayment(record)
				p params
				@zaim.payment(params)
				puts 'ok'
			elsif record['mode'] == 'income' then
				print 'income ...'
				params = paramsForIncome(record)
				p params
				@zaim.income(params)
				puts 'ok'
			elsif record['mode'] == 'transfer' then
				print 'transfer ...'
				params = paramsForTransfer(record)
				p params
				@zaim.transfer(params)
				puts 'ok'
			else
				raise '不明なmode ' + record.to_s
			end
		}
	end
end
