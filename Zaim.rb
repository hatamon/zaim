require 'oauth'
require 'json'

class ZaimException < StandardError
	attr_reader :uri, :code, :message
	def initialize(uri, code, message)
		@uri = uri
		@code = code
		@message = message
	end

	def to_s
		return @uri + ' ' + @code + ' ' + @message
	end
end

class Zaim
private
	def requestGet(uri)
		response = @access_token.get(uri)
		raise ZaimException.new(uri, response.code, response.message) unless response.kind_of?(Net::HTTPOK)
		return JSON.parse(response.body)
	end

	def requestGetUseCache(uri)
		result = @dataCache[uri]
		if result == nil
			result =  requestGet(uri)
			@dataCache[uri] = result
		end
		return result
	end

	def requestPost(uri, *params)
		response = @access_token.post(uri, *params)
		raise ZaimException.new(uri, response.code, response.message) unless response.kind_of?(Net::HTTPOK)
		return JSON.parse(response.body)
	end

	def requestDelete(uri)
		response = @access_token.delete(uri)
		raise ZaimException.new(uri, response.code, response.message) unless response.kind_of?(Net::HTTPOK)
		return JSON.parse(response.body)
	end

public

	def initialize(consumerKey, consumerSecret, accessToken, accessTokenSecret)
		consumer=OAuth::Consumer.new(consumerKey, consumerSecret,
									 :site=>"https://api.zaim.net",
									 :request_token_path=>"/v2/auth/request",
									 :authorize_url=>"https://auth.zaim.net/users/auth",
									 :access_token_path=>"/v2/auth/access")
		@access_token = OAuth::AccessToken.new(consumer, accessToken, accessTokenSecret)

		@dataCache = Hash.new
	end

	def payment(params)
		mapping = "1"
		result = requestPost('https://api.zaim.net/v2/home/money/payment',
							 :mapping=>mapping.to_s,
							 :category_id=>params["categoryId"],
							 :genre_id=>params["genreId"],
							 :amount=>params["amount"],
							 :date=>params["date"],
							 :from_account_id=>params["fromAccountId"],
							 :comment=>params["comment"],
							 :name=>params["name"],
							 :place=>params["place"]
							)
		return result
	end

	def income(params)
		mapping = "1"
		return requestPost('https://api.zaim.net/v2/home/money/income',
							 :mapping=>mapping,
							 :category_id=>params["categoryId"],
							 :amount=>params["amount"],
							 :date=>params["date"],
							 :to_account_id=>params["toAccountId"],
							 :comment=>params["comment"]
							)
	end

	def transfer(params)
		mapping = "1"
		return requestPost('https://api.zaim.net/v2/home/money/transfer',
							 :mapping=>mapping,
							 :category_id=>params["categoryId"],
							 :amount=>params["amount"],
							 :date=>params["date"],
							 :to_account_id=>params["toAccountId"],
							 :from_account_id=>params["fromAccountId"],
							 :comment=>params["comment"]
							)
	end

	def delete(mode, id)
		return requestDelete("https://api.zaim.net/v2/home/money/#{mode}/#{id}")
	end

	def getAccountList()
		result = @dataCache['account']
		result = requestGetUseCache('https://api.zaim.net/v2/home/account') if result == nil
		return result
	end

	def getMoneyList()
		return requestGetUseCache('https://api.zaim.net/v2/home/money')
	end

	def getCategoryList()
		return requestGetUseCache('https://api.zaim.net/v2/home/category')
	end

	def getGenreList()
		return requestGetUseCache('https://api.zaim.net/v2/home/genre')
	end

	def verify()
		return requestGetUseCache('https://api.zaim.net/v2/home/user/verify')
	end
end
