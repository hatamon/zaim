require 'oauth'
require 'pp'
require './zaiminfo'

consumer=OAuth::Consumer.new(CONSUMER_KEY, CONSUMER_SECRET,
							 :site=>"https://api.zaim.net",
							 :request_token_path=>"/v2/auth/request",
							 :authorize_url=>"https://auth.zaim.net/users/auth",
							 :access_token_path=>"/v2/auth/access")
request_token = consumer.get_request_token(:oauth_callback=>"http://google.com")

system('open', request_token.authorize_url)

print "Input OAuth Verifier:"
oauth_verifier = gets.chomp.strip

access_token = request_token.get_access_token(:oauth_verifier => oauth_verifier)

p "token  = #{access_token.token}"
p "secret = #{access_token.secret}"
