require './OinkToZaim'
require './BackupRestore'

if $ARGV.length < 2 then
	puts "usage : ruby #{$0} convert oinkCsvFileName"
	puts "        ruby #{$0} backup outpuFolderName"
	puts "        ruby #{$0} restore inputFolderName"
else
	begin
		if $ARGV[0] == 'convert' then
			puts 'initializing...'
			oinkToZaim = OinkToZaim.new
			puts 'start...'
			oinkToZaim.convert($ARGV[1])
		else
			puts 'initializing...'
			backupRestore = BackupRestore.new
			if $ARGV[0] == 'backup' then
				backupRestore.backup($ARGV[1])
			elsif $ARGV[0] == 'restore' then
				backupRestore.restore($ARGV[1])
			end
		end
	rescue => e
		p e.to_s
	end
end
