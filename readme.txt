■お使いになる前に
	まず、zaimアプリを登録し、consumer_key, consumer_secret を取得してください。
	次に zaiminfo.rb というファイルを作成してください。
	内容は、以下のとおりにしてください
		CONSUMER_KEY='あなたの値'
		CONSUMER_SECRET='あなたの値

	その後、zaimGetAccessToken.rb を実行して、access token を取得してください

	access token を取得したら、zaiminfo.rb に以下を追加してください
		ACCESS_TOKEN = 'あなたの値'
		ACCESS_TOKEN_SECRET = 'あなたの値'

■oinknote からの移行
以下のように実行すると、oinknote からエクスポートした結果から zaim に記帳できます。
	ruby main.rb convert oinknoteFileName

上記を実行する前に、やっておくべきことがあります。
・accountChange.json と categoryChange.json を編集
	oinknote と zaim では費目名や口座名が違うと思います。
	ご自分の環境にあわせてください。

・zaim に口座やカテゴリーを追加する
	上記で編集した口座、費目（カテゴリーやジャンル）を、zaim にログインして追加しておいてください

・oinknote 以外からでも、OinkToZaim.rb や accountChange.json, categoryChange.json をいじれば対応可能と思います

■バックアップ・リストア
以下のように実行すると、zaim の履歴をバックアップできます。
	ruby main.rb backup outputFolderName
履歴以外に、口座一覧、カテゴリー一覧、ジャンル一覧も吸い出します。

以下のように実行すると、zaim の履歴からリストアできます。
	ruby main.rb restoer inputFolderName
リストアは、バックアップフォルダ内の money.json だけを参照し、リストアします。

accounts.json(口座一覧), categories.jon(カテゴリー一覧), genres.json(ジャンル一覧)は
リストアしません。zaim にログインし、手動で登録してください。

■利用にあたって
このソースコードの利用は自由ですが、このソースコードを利用したいかなる結果についても、
はたもんソフト 秦野光博は一切の責任を負わないものとさせていただきます。

2014/04/27
はたもんソフト 秦野光博
hatamonroid@gmail.com
