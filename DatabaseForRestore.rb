require './Database'

class DatabaseForRestore < Database
public
	def initialize(zaimAccountListJson, zaimCategoryListJson, zaimGenreListJson)
		@hashMap = {}
		@zaimAccountListJson = zaimAccountListJson
		@zaimCategoryListJson = zaimCategoryListJson
		@zaimGenreListJson = zaimGenreListJson
	end

	def getAccountLocalId(id)
		return search(@zaimAccountListJson, 'accounts', {'id'=>id}, ['local_id'])
	end

	def getCategoryLocalId(id)
		return search(@zaimCategoryListJson, 'categories', {'id'=>id}, ['local_id'])
	end

	def getGenreLocalId(id)
		return search(@zaimGenreListJson, 'genres', {'id'=>id}, ['local_id'])
	end
end
