class Database
protected
	def search(tableJson, rootKey, searchKeyValues, getKeys)
		result = @hashMap[[rootKey, searchKeyValues]]
		if (result == nil)
			result = Hash.new
			table = tableJson[rootKey]
			table.each {|record|
				found = true
				searchKeyValues.keys.each {|key|
					if(record[key] != searchKeyValues[key])
						found = false
						break
					end
				}
				if(found) then
					getKeys.each {|getKey|
						result[getKey] = record[getKey]
					}
					break
				end
			}
			if(result.length == 0) then
				getKeys.each {|getKey|
					result[getKey] = ''
				}
			end
			@hashMap[[rootKey, searchKeyValues]] = result
		end
		return result
	end
end
