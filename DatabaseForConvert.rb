require './Database'

class DatabaseForConvert < Database
public
	def initialize(zaimAccountListJson, zaimCategoryListJson, zaimGenreListJson, oinkAccountToZaimTableJson, oinkCategoryToZaimTableJson)
		@hashMap = {}
		@zaimAccountListJson = zaimAccountListJson
		@zaimCategoryListJson = zaimCategoryListJson
		@zaimGenreListJson = zaimGenreListJson
		@oinkAccountToZaimTableJson = oinkAccountToZaimTableJson
		@oinkCategoryToZaimTableJson = oinkCategoryToZaimTableJson
	end

	def getAccountId(accountName)
		return search(@zaimAccountListJson, 'accounts', {'name'=>accountName}, ['id', 'local_id'])
	end

	def getCategoryId(mode, categoryName)
		return search(@zaimCategoryListJson, 'categories', {'mode'=>mode, 'name'=>categoryName}, ['id', 'local_id'])
	end

	def getGenreId(categoryId, genreName)
		return search(@zaimGenreListJson, 'genres', {'category_id'=>categoryId, 'name'=>genreName}, ['id', 'local_id'])
	end

	def oinkAccountToZaim(oink)
		return search(@oinkAccountToZaimTableJson, 'accountChange', {'oink'=>oink}, ['zaim'])
	end

	def oinkCategoryToZaim(mode, oink)
		return search(@oinkCategoryToZaimTableJson, 'categoryChange', {'mode'=>mode, 'oink'=>oink}, ['zaimCategory', 'zaimGenre'])
	end
end
